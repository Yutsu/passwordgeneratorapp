const btn = document.querySelector('button');
const btnSupp = document.querySelector('.far.fa-trash-alt');

const errorLength = document.querySelector('.errorLength');
const errorNumber = document.querySelector('.errorNumber');

const uppercase = document.getElementById('uppercaseOption');
const lowercase = document.getElementById('lowercaseOption');
const numberOption = document.getElementById('numberOption');
const symbol = document.getElementById('symbolOption');

const passwordDisplay = document.querySelector('.passwords');


/* CONDITION LENGTH | NUMBERS */
const conditionLength = length => {
    if(length === ""){
        error(errorLength, "Invalid")
        return;
    }
    else if(length < 7){
        error(errorLength, "C'mon I said at least 7")
        return;
    }
    else {
        errorLength.style.display = "none";
        return length;
    }
}
const conditionNumber = number => {
    if(number === ""){
        error(errorNumber, "Invalid");
        return;
    }
    else if(number < 2){
        error(errorNumber, "C'mon I said at least 2 numbers")
        return;
    }
    else{
        errorNumber.style.display = "none"
        return number;
    }
}
const error = (error, text) => {
    error.style.display = "flex";
    error.textContent = text;
    error.style.color = "red";
}


/* OPTIONS */
const lowercaseLetters = "abcdefghijklmnopqrstuvwyxz";
const uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const numberLetters = "0123456789";
const symbolLetters = "&#[(]){|_}-^@°µ$£+§~=:?!;";

const randomLowercase = () => {
    return lowercaseLetters[Math.floor(Math.random() * lowercaseLetters.length)];
}
const randomUppercase = () => {
    return uppercaseLetters[Math.floor(Math.random() * uppercaseLetters.length)];
}
const randomNumber = () => {
    return numberLetters[Math.floor(Math.random() * numberLetters.length)];
}
const randomSymbol = () => {
    return symbolLetters[Math.floor(Math.random() * symbolLetters.length)];
}
const generateLetter = () => {
    const stock = [];
    if(uppercase.checked){
        stock.push(randomUppercase());
    }
    if(lowercase.checked){
        stock.push(randomLowercase());
    }
    if(numberOption.checked){
        stock.push(randomNumber());
    }
    if(symbol.checked){
        stock.push(randomSymbol());
    }
    return stock[Math.floor(Math.random() * stock.length)];
}


/* EVENTS */
btn.addEventListener('click',() => {
    const length = document.getElementById('length').value;
    const number = document.getElementById('numbers').value;

    let check = 0;
    let noCheck = 0;
    let password = "";
    let passwordLength = conditionLength(length);
    let passwordNumber = conditionNumber(number);


    if(uppercase.checked){
        password += randomUppercase();
        check++;
    } else {
        noCheck++;
    }
    if(lowercase.checked){
        password += randomLowercase();
        check++;
    } else {
        noCheck++;
    }
    if(numberOption.checked){
        for(let index = 0; index < number; index++){
            password += randomNumber();
        }
        check++;
    } else {
        noCheck++;
    }
    if(symbol.checked){
        password += randomSymbol();
        check++;
    } else {
        noCheck++;
    }

    if(numberOption.checked && (length-check) < number){
        error(errorNumber, "Too much numbers added");
        return;
    }

    if(noCheck === 4){
        alert("Zero option");
        return;
    }

    if(numberOption.checked == false && number){
        error(errorNumber, "")
        alert("Check number option first");
        return;
    }

    if(passwordLength !== undefined && passwordNumber !== undefined){
        for(let i = password.length; i < passwordLength; i ++){
            const addLetter = generateLetter();
            password += addLetter;
        }

        let flex = document.createElement('div');
        let addDiv = document.createElement('div');
        let copy = document.createElement('button');
        let addLine = document.createElement('div');

        flex.classList.add('flex', 'animate');
        addDiv.classList.add('password');
        copy.classList.add('copy');
        addLine.classList.add('line');

        addDiv.textContent = password;
        copy.innerHTML = "copy"

        flex.appendChild(addDiv);
        flex.appendChild(copy);

        passwordDisplay.appendChild(flex);
        passwordDisplay.appendChild(addLine);

        copy.addEventListener('click', () => {
            const textarea = document.createElement("textarea");
            document.body.appendChild(textarea);
            textarea.value = password;
            textarea.style.opacity = "0";
            //via value
            textarea.select();
            document.execCommand('copy');
            alert(`Password copied in your clipboard : ${password}`);
        })

        btnSupp.addEventListener('click', () => {
            flex.classList.add('flex');
            flex.remove();
            addLine.remove();
        });
    }
});
