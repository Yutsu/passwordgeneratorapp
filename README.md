# Password Generator Application
![Design homepage of Password Generator Application](./images/home.png)

## Description
This is a responsive application where you can have a random password according to several options.

## Contains

Animation when add / delete password.

4 options for the password

Form with length and numbers

Copy and paste the password

Many conditions if not in the criteria

## Technologies Used
Html

Css

Javascript

## Installation
git clone https://gitlab.com/Yutsu/passwordgeneratorapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Markus Spiske**

https://www.pexels.com/photo/green-textile-in-dark-room-193349/
